import { Component } from "react";
import styled from "styled-components";

const LocationDisplayStyle = styled.div`
    width: 100%;
    height: 12%;
    border: 8px ridge #b3b6af;
    border-radius: 14px;
    background: rgb(2, 0, 36);
    background: linear-gradient(
        313deg,
        rgba(2, 0, 36, 1) 0%,
        rgba(9, 9, 121, 1) 37%,
        rgba(26, 76, 185, 1) 100%
    );

    div {
        width: 100%;
        height: 100%;
        text-align: left;
        display: flex;
        align-items: center;
        margin-left: 5%;
    }
`;

class LocationDisplay extends Component {
    render() {
        return (
            <LocationDisplayStyle>
                <div>Sector 7 Slums</div>
            </LocationDisplayStyle>
        );
    }
}

export default LocationDisplay;
