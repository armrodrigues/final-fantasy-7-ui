import { Component } from "react";
import styled from "styled-components";

const TimeCurrencyDisplayStyle = styled.div`
    width: 50%;
    height: 14%;
    border: 8px ridge #b3b6af;
    border-radius: 14px;
    background: rgb(2, 0, 36);
    background: linear-gradient(
        342deg,
        rgba(2, 0, 36, 1) 0%,
        rgba(9, 9, 121, 1) 37%,
        rgba(26, 76, 185, 1) 100%
    );

    table {
        width: 100%;
    }

    th {
        text-align: left;
    }

    th:nth-child(2) {
        text-align: right;
        font-weight: bold;
        color: white;
    }
`;

class TimeCurrencyDisplay extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: new Date(),
        };
        this.getDisplayTime = this.getDisplayTime.bind(this);
    }

    componentDidMount() {
        this.update = setInterval(() => {
            this.setState({ time: new Date() });
        }, 1 * 1000); // every 1 seconds
    }

    componentWillUnmount() {
        clearInterval(this.update);
    }

    time_with_leading_zeros(dt) {
        return (dt < 10 ? "0" : "") + dt;
    }

    getDisplayTime(currentTime) {
        return (
            this.time_with_leading_zeros(currentTime.getHours()) +
            ":" +
            this.time_with_leading_zeros(currentTime.getMinutes()) +
            ":" +
            this.time_with_leading_zeros(currentTime.getSeconds())
        );
    }

    render() {
        return (
            <TimeCurrencyDisplayStyle>
                <table>
                    <tbody>
                        <tr>
                            <th>Time</th>
                            <th>{this.getDisplayTime(this.state.time)}</th>
                        </tr>
                        <tr>
                            <th>Gil</th>
                            <th>1000</th>
                        </tr>
                    </tbody>
                </table>
            </TimeCurrencyDisplayStyle>
        );
    }
}

export default TimeCurrencyDisplay;
