import React, { Component } from "react";
import ProgressBar from "./ProgressBar";
import styled from "styled-components";

const CharacterInfoDisplayStyle = styled.div`
    text-align: left;
    display: grid;
    width: 90%;
    height: 28%;
    margin-top: 1.5%;
    grid-template-columns: 1fr 2fr;

    .info-panel {
        display: grid;
        grid-template-columns: 1fr 1fr;
    }
    .min-stats {
        display: grid;
    }

    .min-stats .stats-area {
        display: grid;
        grid-template-columns: 1fr 5fr;
        width: 90%;
    }

    .hp-mp-area {
        letter-spacing: 2px;
        font-weight: bold;
        text-align: center;
    }

    .portrait {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .portrait img {
        width: 45%;
        height: auto;
    }
    .progress-panel {
        width: 90%;
    }
    .charName {
        grid-column: 1 / span 2;
    }

    .span-color {
        color: #1fd3d9;
    }
`;

class CharacterInfoDisplay extends Component {
    render() {
        const imagePortrait =
            "images/" + this.props.name.toLowerCase() + ".png";
        return (
            <CharacterInfoDisplayStyle>
                <div className="portrait">
                    <img src={imagePortrait} alt={this.props.name} />
                </div>

                <div className="info-panel">
                    <div className="charName">
                        <span>{this.props.name}</span>
                    </div>

                    <div className="min-stats">
                        <div className="stats-area level-area">
                            <div>
                                <span className="span-color">LV</span>
                            </div>
                            <div>
                                <span>{this.props.level}</span>
                            </div>
                        </div>

                        <div className="stats-area hp-mp-area">
                            <div>
                                <span className="span-color">HP</span>
                            </div>
                            <div className="progress-panel">
                                <div>
                                    <span>244/ 311</span>
                                </div>
                                <ProgressBar />
                            </div>
                        </div>

                        <div className="stats-area hp-mp-area">
                            <div>
                                <span className="span-color">MP</span>
                            </div>
                            <div className="progress-panel">
                                <div>
                                    <span>244/ 311</span>
                                </div>
                                <ProgressBar />
                            </div>
                        </div>
                    </div>

                    <div>
                        <div>
                            <span className="">next level</span>
                            <div className="">
                                <ProgressBar barStyle={2} />
                            </div>
                        </div>
                        <div>
                            <span className="">Limit level 1</span>
                            <div className="">
                                <ProgressBar barStyle={2} />
                            </div>
                        </div>
                    </div>
                </div>
            </CharacterInfoDisplayStyle>
        );
    }
}

export default CharacterInfoDisplay;
