import React, { Component } from "react";
import styled from "styled-components";
import MainMenu from "./MainMenu";

const AppStyle = styled.div`
    --appWidth: ${(props) => props.bestDisplaySize.width + "px"};
    --appHeight: ${(props) => props.bestDisplaySize.height + "px"};
    --responsiveFontSize: calc((4.5 * var(--appHeight)) / 100);
    width: var(--appWidth);
    height: var(--appHeight);
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    background-color: black;
    color: #d3d3d3;
    font-family: "Basic", sans-serif;
    text-shadow: 1px 0 0 #000, 0 -1px 0 #000, 0 1px 0 #000, -1px 0 0 #000;
    font-size: var(--responsiveFontSize);
`;

class Menu extends Component {
    render() {
        return (
            <AppStyle bestDisplaySize={this.props.bestDisplaySize}>
                <MainMenu />
            </AppStyle>
        );
    }
}

export default Menu;
