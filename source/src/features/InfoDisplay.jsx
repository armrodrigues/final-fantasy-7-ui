import React, { Component } from "react";
import styled from "styled-components";
import CharacterInfoDisplay from "./CharacterInfoDisplay";

const InfoDisplayStyle = styled.div`
    width: 80%;
    height: 83.333%;
    margin-top: 3%;
    position: relative;
    z-index: 0;
    border: 8px ridge #b3b6af;
    border-radius: 14px;
    background: rgb(2, 0, 36);
    background: linear-gradient(
        342deg,
        rgba(2, 0, 36, 1) 0%,
        rgba(9, 9, 121, 1) 37%,
        rgba(26, 76, 185, 1) 100%
    );
`;

class InfoDisplay extends Component {
    render() {
        return (
            <InfoDisplayStyle>
                <CharacterInfoDisplay name="Cloud" level="8" />
                <CharacterInfoDisplay name="Barret" level="7" />
                <CharacterInfoDisplay name="Tifa" level="5" />
            </InfoDisplayStyle>
        );
    }
}

export default InfoDisplay;
