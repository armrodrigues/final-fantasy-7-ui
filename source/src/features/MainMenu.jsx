import React, { Component } from "react";
import styled from "styled-components";
import InfoDisplay from "./InfoDisplay";
import VerticalMenu from "./VerticalMenu";
import LocationDisplay from "./LocationDisplay";
import TimeCurrencyDisplay from "./TimeCurrencyDisplay";

const MainMenuStyle = styled.div`
    width: 100%;
    height: 100%;
    margin: 0 auto;
    display: flex;
    position: relative;
`;

const Panel = styled.div`
    width: 50%;
    height: 100%;
    display: flex;
    flex: 1;
    flex-direction: column;
    align-items: flex-end;
    z-index: 9;
    position: absolute;
    margin-left: 50%;
`;

class MainMenu extends Component {
    render() {
        return (
            <MainMenuStyle>
                <InfoDisplay />
                <Panel>
                    <VerticalMenu />
                    <TimeCurrencyDisplay />
                    <LocationDisplay />
                </Panel>
            </MainMenuStyle>
        );
    }
}

export default MainMenu;
