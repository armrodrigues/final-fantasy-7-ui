import React, { Component } from "react";
import styled from "styled-components";

const VerticalMenuStyle = styled.div`
    width: 50%;
    height: 65%;
    margin-bottom: 8%;
    border: 8px ridge #b3b6af;
    border-radius: 14px;
    background: rgb(2, 0, 36);
    background: linear-gradient(
        -90deg,
        rgba(2, 0, 36, 1) 0%,
        rgba(9, 9, 121, 1) 37%,
        rgba(26, 76, 185, 1) 100%
    );
`;

const ListDisplay = styled.ul`
    list-style: none;
    text-align: left;
    padding: 0;
    margin-left: 18%;
    margin-top: 3%;
    margin-bottom: 12%;
`;

class VerticalMenu extends Component {
    render() {
        return (
            <VerticalMenuStyle>
                <ListDisplay>
                    <li>Item</li>
                    <li>Magic</li>
                    <li>Materia</li>
                    <li>Equip</li>
                    <li>Status</li>
                    <li>Order</li>
                    <li>Limit</li>
                    <li>Config</li>
                </ListDisplay>

                <ListDisplay>
                    <li>Save</li>
                    <li>Quit</li>
                </ListDisplay>
            </VerticalMenuStyle>
        );
    }
}

export default VerticalMenu;
