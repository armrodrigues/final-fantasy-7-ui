import React, { Component } from "react";
import styled from "styled-components";

const ProgressBarStyle = styled.div`
    width: 100%;
    .frame-bar-1 {
        background-color: #3b0007;
        min-height: 4px;
    }
    .bar-1 {
        height: 2px;
        width: 100%;
        background: rgb(16, 126, 243);
        background: linear-gradient(
            90deg,
            rgba(16, 126, 243, 1) 45%,
            rgba(194, 196, 224, 1) 100%
        );
    }

    .frame-bar-2 {
        margin-left: 10%;
        border: 4px solid #aaaeaf;
        background-color: #5f5f5d;
        width: 86%;
        height: 6px;
    }
    .bar-2 {
        width: 50%;
        height: 100%;
        background-color: #f1c3d0;
        box-shadow: inset 0 0 3em #e6abaf;
    }
`;

class ProgressBar extends Component {
    render() {
        let bar = null;
        switch (this.props.barStyle) {
            case 1:
                bar = "bar-1";
                break;
            case 2:
                bar = "bar-2";
                break;
            default:
                bar = "bar-1";
                break;
        }
        return (
            <ProgressBarStyle>
                <div className={"frame-" + bar}>
                    <div className={bar}></div>
                </div>
            </ProgressBarStyle>
        );
    }
}

export default ProgressBar;
