import React, { Component } from "react";
import Menu from "./features/Menu";

function getWindowDimensions() {
    const { innerWidth: width, innerHeight: height } = window;
    return {
        width,
        height,
    };
}

function calcBestDisplaySize() {
    const resolutions = [
        { width: 128, height: 72 },
        { width: 256, height: 144 },
        { width: 384, height: 216 }, // TODO : fix some margins here, btw its worth the other two?
        { width: 512, height: 288 },
        { width: 640, height: 360 },
        { width: 768, height: 432 },
        { width: 896, height: 504 },
        { width: 1024, height: 576 },
        { width: 1152, height: 648 },
        { width: 1280, height: 720 },
        { width: 1408, height: 792 },
        { width: 1536, height: 864 },
        { width: 1604, height: 936 }, // TODO : from here, borders looks too thim
        { width: 1792, height: 1008 },
        { width: 1920, height: 1080 },
        { width: 2048, height: 1152 },
        { width: 2176, height: 1224 },
        { width: 2304, height: 1296 },
        { width: 2560, height: 1440 },
        { width: 2688, height: 1512 },
        { width: 2816, height: 1584 },
        { width: 2944, height: 1656 },
        { width: 3072, height: 1728 },
        { width: 3200, height: 1800 },
        { width: 3328, height: 1872 },
        { width: 3456, height: 1944 },
        { width: 3584, height: 2016 },
        { width: 3712, height: 2088 },
        { width: 3840, height: 2160 },
    ];

    const size = getWindowDimensions();
    const moduleOffScreen = Math.sqrt((size.width ^ 2) + (size.height ^ 2));

    let auxSize = { width: 0, height: 0 };

    for (var i = 0; i < resolutions.length; i++) {
        const auxModule = Math.sqrt(
            (resolutions[i].width ^ 2) + (resolutions[i].height ^ 2)
        );
        if (Math.trunc(auxModule) > Math.trunc(moduleOffScreen)) {
            if (typeof resolutions[i - 3] === "undefined") {
                auxSize = resolutions[0];
            } else {
                auxSize = resolutions[i - 3];
            }
            break;
        }
    }
    return auxSize;
}

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bestDisplaySize: null,
            isReady: false,
        };
    }

    componentDidMount() {
        const aux = calcBestDisplaySize();
        //alert("The screen size is: " + aux.width + ":" + aux.height);
        this.setState({
            bestDisplaySize: aux,
            isReady: true,
        });
    }

    render() {
        return this.state.isReady ? (
            <Menu bestDisplaySize={this.state.bestDisplaySize} />
        ) : (
            <h1>Loading...</h1>
        );
    }
}

export default App;
